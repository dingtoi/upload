var express = require("express");
var knex = require("../config/knex");
var router = express.Router();
var path = require('path');
var fs = require('fs');

router.post('/transaction/checkNumber', function(req, res){
    var number = req.body.number;
    knex('transactions').select('id').where({'number': number})
    .then(function(rows){
        if(rows.length > 0)
            res.json({status: 200, data: 0});
        else
            res.json({status: 500, data: 1});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.post('/transaction/changeStatus', function(req, res){
    var number = req.body.number;
    var status = req.body.status;
    var device = JSON.stringify(req.body.device);

    knex('transactions')
    .update({
        status: status,
        add_info: device
    })
    .where('number', number)
    .then(function(updated){
        if(updated)
            res.json({status: 200, data: updated});
        else
            res.json({status: 500});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
module.exports = router;